from sqlalchemy import create_engine, Column, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import sqlalchemy
import csv
import requests
import search


CSV_URL = "https://datahub.io/core/airport-codes/r/airport-codes.csv"
Base = declarative_base()


engine = create_engine('mysql://root:root@127.0.0.1:3306/', echo=True)
engine.execute("create database if not exists airports")
engine = create_engine('mysql://root:root@127.0.0.1:3306/airports?use_unicode=1&charset=utf8',
                       echo=False)  # utf8???
if not sqlalchemy.inspect(engine).has_table('airports'):
    search.Airport.metadata.create_all(engine)
else:
    engine.execute((search.Airport.__table__.delete()))

engine.execute("ALTER TABLE airports CONVERT TO CHARACTER SET utf8")
Session = sessionmaker(bind=engine, autoflush=False)

session = Session()
with requests.Session() as s:
    download = s.get(CSV_URL)

    decoded_content = download.content.decode('utf8')

    cr = csv.reader(decoded_content.splitlines(), delimiter=',')
    my_list = list(cr)
    for row in my_list:
        session.merge(search.Airport(row[0], row[-1]))
session.commit()
# airports = session.query(Airport).all()
# for airport in airports:
#    print(airport.id)
session.close()
