#!/bin/bash


#sudo apt update && sudo apt install -y docker.io docker-compose
#sudo systemctl enable docker
#sudo apt-get install libmysqlclient-dev python3-dev
#pyenv install $(cat .python-version)
#pip install -r requirements.txt
docker run -p 3306:3306 --detach --rm --name mysql -e MYSQL_ROOT_PASSWORD=root -v "$(pwd)/data:/var/lib/mysql" mysql:5.7 mysqld --skip-grant-tables &
while !  mysqladmin ping -h"127.0.0.1" --silent; do
    sleep 1
done

python ./import.py
python ./main.py 08G 08GA
sleep 10
docker stop mysql