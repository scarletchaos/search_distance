from math import sin, cos, sqrt, atan2, radians
import search
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
import sys

R = 6373.0


def distance(start, end):
    startLat, startLon = start.split(',')
    endLat, endLon = end.split(',')
    lat1, lon1, lat2, lon2 = radians(float(startLat)), radians(float(startLon)), radians(float(endLat)), radians(
        float(endLon))
    dlon = lon2 - lon1
    dlat = lat2 - lat1

    a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))

    return R * c

# engine = create_engine('mysql://root:root@127.0.0.1:3306/airports?use_unicode=1&charset=utf8',
#                       echo=False)
# Session = sessionmaker(bind=engine)
# session = Session()
# print("Distance test for code: ", sys.argv[1], "and ", sys.argv[2])
# print("The distance between", sys.argv[1], " and ", sys.argv[2], " is ",
#     distance(search.search(sys.argv[1], session), search.search(sys.argv[2], session)))
