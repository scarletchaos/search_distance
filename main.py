from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import search
import distance
import sys


engine = create_engine('mysql://root:root@127.0.0.1:3306/airports?use_unicode=1&charset=utf8',
                       echo=False)
Session = sessionmaker(bind=engine)
session = Session()
print("Search test for codes: ", sys.argv[1], " and ", sys.argv[2])
print(sys.argv[1], " coordinates are ", search.search(sys.argv[1], session), " and ", sys.argv[2], " coordinates are ",
      search.search(sys.argv[2], session))
print("Thus the distance between", sys.argv[1], " and ", sys.argv[2], " is ",
      distance.distance(search.search(sys.argv[1], session), search.search(sys.argv[2], session)))
