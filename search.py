from sqlalchemy import create_engine, Column, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import sys

Base = declarative_base()


class Airport(Base):
    __tablename__ = "airports"

    id = Column('id', String(32), primary_key=True)
    coord = Column('coordinates', String(64), unique=False)

    def __init__(self, id, coord):
        self.id = id
        self.coord = coord


def search(id, session):
    airports = session.query(Airport).all()
    for airport in airports:
        if airport.id == id:
            return airport.coord
    session.close()
    return 0


#engine = create_engine('mysql://root:root@127.0.0.1:3306/airports?use_unicode=1&charset=utf8',
#                       echo=False)
#Session = sessionmaker(bind=engine)
#session = Session()
#print("Search test for code: ", sys.argv[1])
#print(sys.argv[1], "coordinates are: ", search(sys.argv[1], session))
